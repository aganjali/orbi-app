import logger from "redux-logger";
import createSagaMiddleware from "redux-saga";
import {persistStore, persistReducer, PersistConfig} from "redux-persist";
import AsyncStorage from "@react-native-community/async-storage";
import {configureStore, getDefaultMiddleware} from "@reduxjs/toolkit";
import rootSaga from "store/sagas/index";
import rootReducers, {RootState} from "store/reducers";
import {enableReduxLogger} from "helpers/config";

const sagaMiddleware = createSagaMiddleware();

const persistConfig: PersistConfig<RootState> = {
  key: "orbient",
  storage: AsyncStorage,
  whitelist: ["app"],
  blacklist: ["ble"],
};

const persistedReducers = persistReducer<RootState>(
  persistConfig,
  rootReducers,
);

const middleware = [
  sagaMiddleware,
  ...getDefaultMiddleware<RootState, {}>({
    serializableCheck: {ignoredActions: ["persist/PERSIST"]},
    thunk: false,
  }),
];
if (enableReduxLogger) {
  middleware.push(logger);
}

const store = configureStore({
  reducer: persistedReducers,
  middleware,
  devTools: process.env.NODE_ENV !== "production",
});

sagaMiddleware.run(rootSaga);

const persistor = persistStore(store);

export {store, persistor};
