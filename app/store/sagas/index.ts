import {all, fork} from "redux-saga/effects";

import ble from "./ble";

export default function* root() {
  yield all([fork(ble)]);
}

export type SagaRootState = typeof root;
