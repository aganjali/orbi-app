// import {put} from "redux-saga/effects";

import {put} from "redux-saga/effects";
import {log} from "store/reducers/ble";
import {BleStatusCode} from "types/store/ble";

export default function* bleSaga() {
  yield put(log(BleStatusCode.Started));
}
