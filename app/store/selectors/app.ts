import {RootState} from "store/reducers";

export const getI18nConfig = (state: RootState) => state.app.i18n;
export const getThemeConfig = (state: RootState) => state.app.theme;
