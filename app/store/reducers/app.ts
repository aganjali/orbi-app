import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IAppState, I18nConfig, ThemeConfig} from "types/store/app";

const initialState: IAppState = {
  i18n: {isRTL: true, lang: "fa"},
  theme: {
    theme: "light",
    system: true,
  },
};

const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    changeI18nConfig(state: IAppState, action: PayloadAction<I18nConfig>) {
      const {isRTL, lang} = action.payload;
      state.i18n.lang = lang;
      state.i18n.isRTL = isRTL;
    },
    toggleThemeByUser(state: IAppState) {
      const {theme} = state.theme;
      state.theme = {
        theme,
        system: false,
      };
      if (theme === "light") {
        state.theme.theme = "dark";
      } else if (theme === "dark") {
        state.theme.theme = "light";
      }
    },
    setTheme(state: IAppState, action: PayloadAction<ThemeConfig>) {
      state.theme = action.payload;
    },
  },
});

export const {changeI18nConfig, toggleThemeByUser, setTheme} = appSlice.actions;

export default appSlice.reducer;
