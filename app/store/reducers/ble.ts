import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IBleState, BleStatusCode} from "types/store/ble";

const initialState: IBleState = {statusCode: BleStatusCode.None};

const appSlice = createSlice({
  name: "ble",
  initialState,
  reducers: {
    log: (state: IBleState, code: PayloadAction<BleStatusCode>) => {
      state.statusCode = code.payload;
      return state;
    },
  },
});

export const {log} = appSlice.actions;

export default appSlice.reducer;
