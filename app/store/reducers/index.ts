import {combineReducers} from "redux";

import appReducer from "./app";
import bleReducer from "./ble";

const rootReducer = combineReducers({
  app: appReducer,
  ble: bleReducer,
});
export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
