import React from "react";
import {NavigationContainer} from "@react-navigation/native";
import {createNativeStackNavigator} from "react-native-screens/native-stack";
import {useTheme} from "react-native-paper";
import HomeScreen from "view/pages/home";
import Splash from "view/pages/splash";
import * as themes from "view/style/themes/navigation";

const Stack = createNativeStackNavigator();

const Router = () => {
  const [isCompleted, setCompleted] = React.useState(false);
  const {dark} = useTheme();

  const navigationTheme = dark ? themes.dark : themes.light;

  if (!isCompleted) {
    return (
      <Splash
        onComplete={() => {
          setCompleted(true);
        }}
      />
    );
  }
  return (
    <NavigationContainer theme={navigationTheme}>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={HomeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Router;
