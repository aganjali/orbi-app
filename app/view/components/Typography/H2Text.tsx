import * as React from "react";
import {TextProps, StyleSheet} from "react-native";

import StyledText from "./StyledText";

type Props = TextProps & {
  children?: React.ReactNode;
};

const H2Text = (props: Props) => (
  <StyledText {...props} family="bold" style={[styles.text, props.style]} />
);

export default H2Text;

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    fontStyle: "normal",
    lineHeight: 40,
    letterSpacing: 0,
    textAlign: "center",
  },
});
