import * as React from "react";
import {I18nManager, StyleProp, TextStyle} from "react-native";
import {useTheme, Text} from "react-native-paper";

type Props = React.ComponentProps<typeof Text> & {
  textColor?: string;
  family?: "black" | "bold" | "regular" | "medium" | "light" | "thin";
  style?: StyleProp<TextStyle>;
};

const StyledText = (props: Props) => {
  const {
    colors: {orbi},
    fonts,
  } = useTheme();
  const {textColor = orbi.text, family = "regular", style, ...rest} = props;

  const font = fonts[family];
  const writingDirection = I18nManager.isRTL ? "rtl" : "ltr";

  return (
    <Text
      {...rest}
      style={[
        {color: textColor, ...font, textAlign: "right", writingDirection},
        style,
        props.style,
      ]}
    />
  );
};

export default StyledText;
