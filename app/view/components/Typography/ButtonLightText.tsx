import * as React from "react";
import {TextProps, StyleSheet} from "react-native";
import {useTheme} from "react-native-paper";

import StyledText from "./StyledText";

type Props = TextProps & {
  children?: React.ReactNode;
};

const ButtonLightText = (props: Props) => {
  const {
    colors: {orbi},
  } = useTheme();

  return (
    <StyledText
      {...props}
      textColor={orbi.textLight}
      family="medium"
      style={[styles.text, props.style]}
    />
  );
};

export default ButtonLightText;

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: 0,
  },
});
