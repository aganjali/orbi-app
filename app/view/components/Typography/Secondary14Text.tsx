import * as React from "react";
import {TextProps, StyleSheet} from "react-native";

import StyledText from "./StyledText";

type Props = TextProps & {
  children?: React.ReactNode;
};

const Secondary14Text = (props: Props) => (
  <StyledText {...props} family="medium" style={[styles.text, props.style]} />
);

export default Secondary14Text;

const styles = StyleSheet.create({
  text: {
    fontSize: 14,
    fontStyle: "normal",
    lineHeight: 20,
    letterSpacing: 0,
  },
});
