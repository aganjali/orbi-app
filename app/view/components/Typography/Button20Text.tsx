import * as React from "react";
import {TextProps, StyleSheet} from "react-native";
import {useTheme} from "react-native-paper";

import StyledText from "./StyledText";

type Props = TextProps & {
  children?: React.ReactNode;
};

const Button20Text = (props: Props) => {
  const {
    colors: {orbi},
  } = useTheme();

  return (
    <StyledText
      {...props}
      textColor={orbi.textDark}
      family="bold"
      style={[styles.text, props.style]}
    />
  );
};

export default Button20Text;

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: 0,
    textAlign: "center",
  },
});
