import * as React from "react";
import {TextProps, StyleSheet} from "react-native";
import {useTheme} from "react-native-paper";

import StyledText from "./StyledText";

type Props = TextProps & {
  children?: React.ReactNode;
};

const Button18Text = (props: Props) => {
  const {
    colors: {orbi},
  } = useTheme();

  return (
    <StyledText
      {...props}
      textColor={orbi.textDark}
      family="bold"
      style={[styles.text, props.style]}
    />
  );
};

export default Button18Text;

const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    fontStyle: "normal",
    lineHeight: 40,
    letterSpacing: 0,
  },
});
