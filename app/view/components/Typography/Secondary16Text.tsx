import * as React from "react";
import {TextProps, StyleSheet} from "react-native";

import StyledText from "./StyledText";

type Props = TextProps & {
  children?: React.ReactNode;
};

const Secondary16Text = (props: Props) => (
  <StyledText {...props} family="medium" style={[styles.text, props.style]} />
);

export default Secondary16Text;

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: 0,
  },
});
