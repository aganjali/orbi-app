import * as React from "react";
import {TextProps, StyleSheet} from "react-native";

import StyledText from "./StyledText";

type Props = TextProps & {
  children?: React.ReactNode;
};

const H1Text = (props: Props) => (
  <StyledText {...props} family="bold" style={[styles.text, props.style]} />
);

export default H1Text;

const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    fontStyle: "normal",
    lineHeight: 24,
    letterSpacing: 0,
  },
});
