import React from "react";
import {
  StyleSheet,
  View,
  ActivityIndicator,
  ViewProps,
  ViewStyle,
  StyleProp,
} from "react-native";

interface Props extends ViewProps {
  size?: number | "small" | "large";
  color?: string;
  style?: StyleProp<ViewStyle>;
}
const Loading: React.FC<Props> = ({size, color, style, ...viewProps}) => {
  return (
    <View style={[styles.container, style]} {...viewProps}>
      <ActivityIndicator size={size} color={color} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignSelf: "center",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
  },
});

export default Loading;
