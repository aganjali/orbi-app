import React from "react";
import {View, StyleSheet, StyleProp, ViewStyle, Pressable} from "react-native";
import {useTheme} from "react-native-paper";

import Button18Text from "./Typography/Button18Text";

interface Props {
  style?: StyleProp<ViewStyle>;
  contentStyle?: StyleProp<ViewStyle>;
  text: string;
  onPress: () => void;
}

const Button = ({style, contentStyle, text, onPress}: Props) => {
  const {
    colors: {orbi},
    spacing,
  } = useTheme();

  return (
    <Pressable
      onPress={onPress}
      style={({pressed}) => [
        styles.container,
        {backgroundColor: orbi.primary200, opacity: pressed ? 0.5 : 1},
        style,
      ]}>
      <View
        style={[
          styles.content,
          orbi.button,
          {paddingHorizontal: spacing.xxl},
          contentStyle,
        ]}>
        <Button18Text>{text}</Button18Text>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 4,
    height: 60,
    borderRadius: 46,
  },
  content: {
    flex: 1,
    borderRadius: 36,
    borderStyle: "solid",
    borderWidth: 4,
    justifyContent: "center",
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,

    elevation: 12,
  },
});

export default React.memo(Button);
