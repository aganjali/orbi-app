export {default as Loading} from "./Loading";
export {default as Button} from "./Button";

export {default as BodyText} from "./Typography/BodyText";
export {default as Button18Text} from "./Typography/Button18Text";
export {default as Button20Text} from "./Typography/Button20Text";
export {default as ButtonLightText} from "./Typography/ButtonLightText";
export {default as H1Text} from "./Typography/H1Text";
export {default as H2Text} from "./Typography/H2Text";
export {default as Secondary14Text} from "./Typography/Secondary14Text";
export {default as Secondary16Text} from "./Typography/Secondary16Text";
export {default as StyledText} from "./Typography/StyledText";
