import React from "react";
import {View, StyleSheet} from "react-native";
import {Button} from "view/components";
import {t} from "i18n";

const Home = ({}) => {
  return (
    <View style={styles.container}>
      <Button text={t("app.appName")} onPress={() => {}} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
  },
});

export default Home;
