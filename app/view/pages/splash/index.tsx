import React, {useEffect, useState} from "react";
import {StyleSheet, View, Image, Platform} from "react-native";
import {SafeAreaView} from "react-native-safe-area-context";
import {useTheme} from "react-native-paper";
import RNRestart from "react-native-restart";
import AsyncStorage from "@react-native-community/async-storage";
import {Loading} from "view/components";
import {logger} from "helpers/utils";

let tid: NodeJS.Timeout;
interface Props {
  onComplete: () => void;
}
type CheckListKeys = "init";
type CheckList = {[key in CheckListKeys]: boolean};

const Splash = ({onComplete}: Props) => {
  const {colors} = useTheme();
  const [checkList, setCheckList] = useState<CheckList>({
    init: false,
  });

  useEffect(() => {
    checkFirstInit();
    return () => {
      clearTimeout(tid);
    };
  }, []);

  useEffect(() => {
    const res = Object.keys(checkList).every(
      (e) => checkList[e as CheckListKeys],
    );
    if (res) {
      onComplete();
    }
  }, [checkList, onComplete]);

  const checkFirstInit = async () => {
    try {
      const value = await AsyncStorage.getItem("first_init");

      if (!value) {
        await AsyncStorage.setItem("first_init", "true");
        // if (Platform.OS === "android") {
        tid = setTimeout(() => {
          RNRestart.Restart();
        }, 2000);

        // }
      } else setCheckList((prev) => ({...prev, init: true}));
    } catch (e) {
      logger(e);
      setCheckList((prev) => ({...prev, init: true}));
    } finally {
    }
  };

  return (
    <SafeAreaView
      style={[styles.safeAreaView, {backgroundColor: colors.background}]}>
      <View style={styles.main}>
        <View style={styles.logoContainer}>
          <Image
            source={require("assets/images/logo.png")}
            resizeMode="contain"
          />
        </View>
      </View>
      <Loading style={styles.loading} color={colors.primary} size={40} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
  main: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  logoContainer: {flexDirection: "row", alignItems: "center"},
  loading: {marginBottom: 40},
});
export default Splash;
