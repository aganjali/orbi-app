import {Platform} from "react-native";
import {Colors} from "react-native-paper";
import color from "color";

import {defaultFontConfig} from "./fonts";

const theme: ReactNativePaper.Theme = {
  dark: false,
  roundness: 4,
  spacing: {
    xxs: 6,
    xs: 8,
    s: 12,
    m: 16,
    l: 20,
    xl: 24,
    xxl: 32,
  },
  colors: {
    primary: "#251f52",
    accent: "#00d9ff",
    background: "#565b9f",
    surface: "#565b9f",
    error: "#f16361",
    text: Colors.white,
    onBackground: "#000000",
    onSurface: "#000000",
    disabled: color(Colors.black).alpha(0.26).rgb().string(),
    placeholder: "#cacaca",
    backdrop: color(Colors.black).alpha(0.5).rgb().string(),
    notification: Colors.pinkA400,
    orbi: {
      primary100: "#565b9f",
      primary200: "#4a4d8d",
      primary400: "#41427f",
      primary500: "#3b3b75",
      primary700: "#323067",
      primary900: "#251f52",

      accent: "#00d9ff",

      backgroundLight: "#565b9f",
      backgroundDark: "#16163b",

      text: "#ffffff",
      textLight: "#d2dcee",
      textDark: "#3c3b78",

      button: {borderColor: "#d2dcee", backgroundColor: "#ffffff"},
      drift: "#00daff",
      n2: "#00ff99",

      calibrate: [],
      update: [],
      battery: {low: [], medium: [], high: []},

      slider1: ["#ffff00", "#ff6700"],
      slider2: ["#00e1ff", "#ea00ff"],
    },
  },
  animation: {
    scale: 1.0,
  },
  fonts: Platform.select(defaultFontConfig) as ReactNativePaper.ThemeFonts,
};
export default theme;
