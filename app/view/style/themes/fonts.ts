import {PlatformOSType} from "react-native";

type FontConfig = {
  [platform in PlatformOSType | "default"]?: ReactNativePaper.ThemeFonts;
};

export const defaultFontConfig: FontConfig = {
  default: {
    thin: {
      fontFamily: "Vazir-Thin-FD",
      fontWeight: "normal",
    },
    light: {
      fontFamily: "Vazir-Light-FD",
      fontWeight: "normal",
    },
    regular: {
      fontFamily: "Vazir-FD",
      fontWeight: "normal",
    },
    medium: {
      fontFamily: "Vazir-Medium-FD",
      fontWeight: "normal",
    },
    bold: {
      fontFamily: "Vazir-Bold-FD",
      fontWeight: "normal",
    },
    black: {
      fontFamily: "Vazir-Black-FD",
      fontWeight: "normal",
    },
  },
};
