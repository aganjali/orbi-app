import {DarkTheme} from "@react-navigation/native";

const Theme = {
  ...DarkTheme,
  dark: true,
  colors: {
    ...DarkTheme.colors,
    background: "#565b9f",
  },
};

export default Theme;
