import {DefaultTheme} from "@react-navigation/native";

const LightTheme = {
  ...DefaultTheme,
  dark: false,
  colors: {
    ...DefaultTheme.colors,
    background: "#565b9f",
  },
};

export default LightTheme;
