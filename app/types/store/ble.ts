export enum BleStatusCode {
  None,
  Started,
}
export interface IBleState {
  statusCode: BleStatusCode;
}
