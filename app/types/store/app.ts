export type I18nConfig = {
  isRTL: boolean;
  lang: "en" | "fa";
};
export type Theme = "light" | "dark";
export type ThemeConfig = {
  theme: Theme;
  system: boolean;
};
export interface IAppState {
  i18n: I18nConfig;
  theme: ThemeConfig;
}
