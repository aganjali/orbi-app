declare global {
  namespace ReactNativePaper {
    interface ThemeFont {}
    interface ThemeFonts {
      bold: ThemeFont;
      black: ThemeFont;
    }
    interface ThemeColors {
      orbi: OrbiColors;
    }
    interface ThemeAnimation {}
    interface Theme {
      spacing: Spacing;
    }
  }
}
export type Spacing = {
  xxs: number;
  xs: number;
  s: number;
  m: number;
  l: number;
  xl: number;
  xxl: number;
};

export type OrbiColors = {
  primary100: string;
  primary200: string;
  primary400: string;
  primary500: string;
  primary700: string;
  primary900: string;

  accent: string;

  backgroundLight: string;
  backgroundDark: string;

  text: string;
  textLight: string;
  textDark: string;

  button: {
    backgroundColor: string;
    borderColor: string;
  };
  drift: string;
  n2: string;

  calibrate: string[];
  battery: {low: string[]; medium: string[]; high: string[]};
  update: string[];
  slider1: [string, string];
  slider2: [string, string];
};

export interface OrbiTheme extends ReactNativePaper.Theme {}
