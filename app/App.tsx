import React, {useEffect, useState, useLayoutEffect} from "react";
import {StatusBar, useColorScheme} from "react-native";
import {Provider as PaperProvider} from "react-native-paper";
import {useSelector, useDispatch} from "react-redux";
import Navigator from "routes";
import RNRestart from "react-native-restart";
import * as themes from "view/style/themes";
import {getThemeConfig, getI18nConfig} from "store/selectors/app";
import {setTheme} from "store/reducers/app";
import {setI18nConfig} from "i18n";

const App = () => {
  const dispatch = useDispatch();
  const colorSchema = useColorScheme();
  const themeConfig = useSelector(getThemeConfig);
  const {theme, system: isSystemTheme} = themeConfig;

  const [loaded, setLoaded] = useState(false);
  const config = useSelector(getI18nConfig);
  useLayoutEffect(() => {
    setI18nConfig(config);
  }, [config]);

  useEffect(() => {
    if (loaded) {
      requestAnimationFrame(() => RNRestart.Restart());
    }
    setLoaded(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [config]);

  useEffect(() => {
    if (isSystemTheme && colorSchema && theme !== colorSchema) {
      dispatch(setTheme({...themeConfig, theme: colorSchema}));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [themeConfig, colorSchema, dispatch]);

  const t = isSystemTheme && colorSchema ? colorSchema : theme;
  return (
    <PaperProvider theme={themes[t]}>
      <StatusBar barStyle={t === "light" ? "dark-content" : "light-content"} />
      <Navigator />
    </PaperProvider>
  );
};

export default App;
