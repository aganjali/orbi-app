import I18n from "i18n-js";
import {I18nManager} from "react-native";
import {I18nConfig} from "types/store/app";

import {changeI18nConfig} from "../store/reducers/app";
import {store} from "../store";

import * as i18nEN from "./en";
import * as i18nFA from "./fa";

I18n.translations = {
  en: {
    app: i18nEN.appLang,
  },
  fa: {
    app: i18nFA.appLang,
  },
};
I18n.fallbacks = "en";

const t = (scope: I18n.Scope, options?: I18n.TranslateOptions) =>
  I18n.t(scope, options);

const setI18nConfig = (config: I18nConfig) => {
  const state = store.getState();
  const {isRTL = false, lang} = config ? config : state.app.i18n;
  store.dispatch(changeI18nConfig({isRTL, lang}));

  I18nManager.forceRTL(isRTL);

  I18n.locale = lang;
};

export {setI18nConfig, t};
